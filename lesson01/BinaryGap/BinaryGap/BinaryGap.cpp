// BinaryGap.cpp : Counts the max number of zeroes between two ones in an int
//

#include <iostream>
#include <random>
#include <sstream>


int solution(int in_number)
{
	size_t length = sizeof(in_number) * CHAR_BIT;
	int currentVal(in_number);
	int zeroCountMax(0);
	int zeroCountCurrent(0);
	bool haveOne(false);
	
	for (unsigned int i(0); i < length && currentVal > 0; ++i)
	{
		bool one = (currentVal & 1);

		if (one)
		{
			if (zeroCountCurrent > zeroCountMax)
				zeroCountMax = zeroCountCurrent;

			zeroCountCurrent = 0;
			haveOne = true;
		}
		else
		{
			if(haveOne)
				++zeroCountCurrent;
		}

		currentVal >>= 1;
	}

	return zeroCountMax;
}

void Test(int in_numTests = 32)
{
	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(0, 64);// INT_MAX);
	
	for (int i(0); i < in_numTests; ++i)
	{
		int dice_roll = distribution(generator);
		int zeroGap = solution(dice_roll);
		
		std::cout << "Test (" << dice_roll << ") : ";
		
		std::stringstream ss;		

		while (dice_roll > 0)
		{
			ss << (dice_roll & 1);
			dice_roll >>= 1;
		}

		std::string bits(ss.str());
		std::string reversed(bits.rbegin(), bits.rend());

		std::cout << reversed;
		std::cout << ", gap : " << zeroGap << std::endl;
	}
}

int main()
{
	Test(32);

	int wait;
	std::cin >> wait;

    return 0;
}

