// cyclicRotation.cpp : Shifts elements in an array right by a specified amount.
//

#include <iostream>


struct Results
{
	int* A;
	int length;
};

struct Results RightRotateArray(int in_A[], int in_length)
{
	Results ret;

	if (in_length == 0)
	{
		ret.A = in_A;
		ret.length = in_length;
		return ret;
	}

	int carry = in_A[in_length-1];

	for (int i(0); i < in_length; ++i)
	{
		int current = in_A[i];
		in_A[i] = carry;
		carry = current;
	}

	ret.A = in_A;
	ret.length = in_length;

	return ret;
}

void PrintArray(int in_A[], int in_length)
{
	for (int i(0); i < in_length; ++i)
	{
		std::cout << in_A[i];

		if (i < in_length - 1)
			std::cout << ",";
	}

	std::cout << std::endl;
}

struct Results solution(int in_A[], int in_length, int in_rightRotations)
{
	in_rightRotations %= in_length;

	Results res = { in_A, in_length };

	for (int i(0); i < in_rightRotations; ++i)
		res = RightRotateArray(res.A, res.length);

	return res;
}

int main()
{
	const int k_len = 10;
	
	int* A = new int[k_len];

	std::cout << "Input ";

	for (int i(0); i < k_len; ++i)
		A[i] = i;

	PrintArray(A, k_len);

	Results res = solution(A, k_len, 16);

	std::cout << "Output ";
	PrintArray(res.A, res.length);

	int wait;
	std::cin >> wait;

	delete[] A;

    return 0;
}

