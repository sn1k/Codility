﻿using System;
using System.Collections.Generic;

namespace countSemiPrimes
{
    class Solution
    {
        int[] GeneratePrimes(int in_max)
        {
            bool[] notPrimes = new bool[in_max]; // def false
            
            int sieveIndices = (int)Math.Ceiling(Math.Sqrt(in_max));

            // build products for each index and we'll block out the array until only primes remain. starting from 2 (primes ignore 1)
            for (int i = 2; i < sieveIndices; ++i)
            {
                if (notPrimes[i] == true) // already flagged
                    continue;

                // build sieve for this value i
                for (int j = i*2; j < in_max; j += i)
                {
                    notPrimes[j] = true;
                }
            }

            // return spaces
            List<int> primes = new List<int>(sieveIndices);
            
            for (int i = 2; i < notPrimes.Length; ++i)
            {
                if (notPrimes[i] == false)
                    primes.Add(i);
            }

            return primes.ToArray();
        }

        /// <param name="in_primes"></param>
        /// <param name="in_maxSubPrimeVal">filter value, entries are lte</param>
        /// <returns>array of prime products</returns>
        int[] GenerateSemiPrimes(int[] in_primes, int in_maxSubPrimeVal)
        {
            List<int> semiPrimes = new List<int>(in_primes.Length * 2);

            for (int i = 0; i < in_primes.Length; ++i)
            {
                for (int j = i; j < in_primes.Length; ++j)
                {
                    int subPrime = in_primes[i] * in_primes[j];

                    if (subPrime <= in_maxSubPrimeVal)
                        semiPrimes.Add(subPrime);
                }
            }

            return semiPrimes.ToArray();
        }

        /// <summary>
        /// Given an integer in_maxSemiprime and two non-empty arrays in_rangeStarts and in_rangeEnds consisting of M integers,
        /// returns an array consisting of M elements specifying the consecutive answers to all the queries.
        /// </summary>
        /// <param name="in_maxSemiprime"></param>
        /// <param name="in_rangeStarts"></param>
        /// <param name="in_rangeEnds"></param>
        /// <returns>array of subprime counts for the given ranges</returns>
        public int[] solution(int in_maxSemiprime, int[] in_rangeStarts, int[] in_rangeEnds)
        {
            int[] solutions = new int[in_rangeStarts.Length];

            // generate primes (maxVal/2 as 2 is smallest prime, any other products are outside of requested ranges)
            int[] primes = GeneratePrimes(in_maxSemiprime);

            // generate semiprimes
            int[] semiPrimes = GenerateSemiPrimes(primes, in_maxSemiprime);

            int semiPrimeCount;

            // run ranges
            for (int i = 0; i < in_rangeStarts.Length; ++i)
            {
                semiPrimeCount = 0;
                int rangeStart = in_rangeStarts[i];
                int rangeEnd = in_rangeEnds[i];

                // not happy with complexity here either
                for (int j = 0; j < semiPrimes.Length; ++j)
                {
                    // range check and increment counter if valid
                    if (semiPrimes[j] >= rangeStart && semiPrimes[j] <= rangeEnd)
                        ++semiPrimeCount;
                }

                solutions[i] = semiPrimeCount;
            }

            return solutions;
        }
    }

    class Program
    {
        
        static void Main(string[] args)
        {
            int[] startRanges = {1, 4, 16 };
            int[] endRanges = { 26, 10, 20};

            const int k_maxSemiPrime = 26;

            Solution s = new Solution();

            int[]  result = s.solution(k_maxSemiPrime, startRanges, endRanges);

            Console.WriteLine("Result: " + string.Join(",", result));
            Console.In.Read();
        }
    }
}
