// tapeEquilibrium.cpp : Find index in array where sum of before and equal/after values is minimised
//


#include <iostream>
#include <algorithm>

int solution(int in_array[], int in_length)
{
	if (in_length <= 1)
		return 0;

	long long total(0);

	for (int i(0); i < in_length; ++i)
		total += in_array[i];

	int runningTotal(in_array[0]);

	total -= in_array[0];
	
	int lastDiff(total - runningTotal);

	for (int i(1); i < in_length; ++i)
	{
		// get diff
		runningTotal += in_array[i];
		total -= in_array[i];

		long long diff(total - runningTotal);
		
		// if diff now negative, return last or current value
		if(diff < 0)
			return abs(diff) < abs(lastDiff) ? diff : lastDiff;
		
		lastDiff = diff;
	}

	return 0;
}

int main()
{
	const int k_len(8);

	int* ar = new int[k_len];

	for (int i(0); i < k_len; ++i)
		ar[i] = i;// +(rand() % 2);

	// shuffle values
	std::random_shuffle(&ar[0], &ar[k_len - 1]);

	for (int i(0); i < k_len; ++i)
	{
		std::cout << ar[i] << ',';
	}
	std::cout << std::endl;

	int sol = solution(ar, k_len);

	std::cout << "solution: " << sol << std::endl;

	int wait;
	std::cin >> wait;

    return 0;
}

