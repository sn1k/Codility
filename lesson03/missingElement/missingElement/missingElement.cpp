// missingElement.cpp : Given an array of values 1 .. n+1, find the single missing value
//

#include <iostream>
#include <algorithm>

int solution(int in_array[], int in_length)
{
	long long total((in_length+1+1)*((in_length+1)*0.5));
	
	for (int i(0); i < in_length; ++i)
		total -= in_array[i];
			

	return total;
}

int main()
{
	const int k_len(10000);
	const int k_missingIndex(16);

	int* ar = new int[k_len];

	for (int i(0); i < k_len; ++i)
		ar[i] = i+1;

	ar[k_missingIndex] = k_len + 1;

	std::random_shuffle(&ar[0], &ar[k_len-1]);

	int sol = solution(ar, k_len);
	std::cout << "missing " << (k_missingIndex +1) << std::endl;
	std::cout << "sol " << sol << std::endl;

	int wait;
	std::cin >> wait;

	delete[] ar;
	
    return 0;
}

