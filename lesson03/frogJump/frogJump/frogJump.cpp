// frogJump.cpp : Defines the entry point for the console application.
//

#include <iostream>

int solution(int in_X, int in_Y, int in_jumpLength)
{
	int travel(in_Y - in_X);
	return travel / in_jumpLength + (travel % in_jumpLength != 0);
}

int main()
{
	int jumps = solution(5, 25, 6);

	std::cout << "vals(x,y,jumplen) " << 5 << ", " << 25 << ", jumplen " << 6 << " is " << jumps << std::endl;
	
	int wait;
	std::cin >> wait;
    return 0;
}

