﻿using System;
using System.Collections.Generic;

//Given an array[seconds] = position, find the earliest time we have a full positional run

namespace frogRiver1CSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Solution s = new Solution();
            s.Test();

            Console.In.Read();
        }
    }

    class Solution
    {
        public void Test()
        {
            // index gives time, values are positions
            const int k_numTests = 3;

            int[][] tests = new int[k_numTests][];

            int[] test0 = { 1, 3, 1, 4, 2, 3, 5, 4 }; // succeed  (7s)
            int[] test1 = { 1, 9, 14, 1, 3, 3, 1, 4, 8, 9, 1, 4, 3, 2, 3, 5, 4 }; // succeed (16s)
            int[] test2 = { 1, 9, 14, 1, 3, 3, 1, 4, 8, 9, 1, 3, 2, 3, 4, 7, 6 }; // fail

            tests[0] = test0;
            tests[1] = test1;
            tests[2] = test2;

            const int k_destination = 5;

            for (int i = 0; i < k_numTests; ++i)
            {
                int time = solution(k_destination, tests[i]);

                if (time == -1)
                    Console.WriteLine(String.Format("input {0}, solution cannot be found", string.Join(",", tests[i])));
                else
                    Console.WriteLine(String.Format("input {0}, solution {1}s", string.Join(",", tests[i]), time));
            }

        }

        /// <summary>
        /// Function returns the 
        /// </summary>
        /// <param name="in_dist">target position to path to</param>
        /// <param name="in_positions">viable positions at index time</param>
        /// <returns>index+1 (time) that a path to in_dist is complete</returns>
        public int solution(int in_dist, int[] in_positions)
        {
            SortedList<int, int> sortedList = new SortedList<int, int>();

            // how far our path goes currently
            int travelCounter = 0;
            
            for (int i = 0; i < in_positions.Length; ++i)
            {
                int positionToCheck = in_positions[i];

                // desired next step
                if (positionToCheck == travelCounter + 1)
                {
                    ++travelCounter;

                    // completion check
                    if (travelCounter == in_dist)
                        return i+1;                   
                }
                // else if it's a usable future step, push to sorted list
                else if (positionToCheck <= in_dist && positionToCheck > travelCounter)
                {
                    // ignore duplicates
                    if (sortedList.IndexOfKey(positionToCheck) == -1)
                    {
                        sortedList.Add(positionToCheck, positionToCheck);
                        continue;
                    }
                }

                // check for valid steps already held in sorted list
                while (sortedList.Count > 0 && sortedList.IndexOfKey(travelCounter + 1) != -1)
                {
                    int indexOfNextVal = sortedList.IndexOfKey(travelCounter + 1);

                    sortedList.RemoveAt(indexOfNextVal);
                    ++travelCounter;
                    
                    // completion check
                    if (travelCounter == in_dist)
                        return i+1;
                }   
            }

            return -1;
        }
    }
}
